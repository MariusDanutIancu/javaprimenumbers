import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 */
public class Prime
{
    private static final BigInteger MIN_PRIME = BigInteger.TWO;
    private static final BigInteger BIGINTEGER_THREE = BigInteger.valueOf(3);

    /**
     * Returns an aproximate square root.
     *
     * @param n Input number.
     * @return Square root.
     */
    private static BigInteger sqrtN(BigInteger n)
    {
        BigInteger half = n.shiftRight(1);
        while (half.multiply(half).compareTo(n) > 0)
        {
            half = half.shiftRight(1);
        }
        return half.shiftLeft(1);
    }

    /**
     * Basic prime check function.
     *
     * @param number Input integer.
     * @return True if nummber is prime. False, otherwise
     */
    private static boolean isPrime(BigInteger number)
    {
        if(number.compareTo(MIN_PRIME) < 0)
        {
            return false;
        }

        if (!BigInteger.TWO.equals(number) && BigInteger.ZERO.equals(number.mod(BigInteger.TWO)))
        {
            return false;
        }

        BigInteger root = sqrtN(number);
        for(BigInteger i = BIGINTEGER_THREE; i.compareTo(root) <= 0; i = i.add(BigInteger.TWO))
        {
            if(BigInteger.ZERO.equals(number.mod(i)))
            {
                return false;
            }
        }

        return true;
    }

    /**
     * Optimized prime check for bigintegers using Miller-Rabin test.
     *
     * @param number Input integer.
     * @return True if number is prime. False, otherwise.
     */
    private static boolean isPrimeBi(BigInteger number)
    {
        // checks if number is composite
        if (!number.isProbablePrime(10))
        {
            return false;
        }

        if (!BigInteger.TWO.equals(number) && BigInteger.ZERO.equals(number.mod(BigInteger.TWO)))
        {
            return false;
        }

        BigInteger root = sqrtN(number);
        for (BigInteger i = BIGINTEGER_THREE; i.compareTo(root) < 1; i = i.nextProbablePrime())
        {
            if (BigInteger.ZERO.equals(number.mod(i)))
            {
                return false;
            }
        }
        return true;
    }

    /**
     *
     * @param number Input integer.
     * @return Next prime number.
     */
    private static BigInteger nextPrime(BigInteger number)
    {
        if (number.compareTo(MIN_PRIME) < 0)
        {
            return BigInteger.TWO;
        }

        BigInteger iNum = number.add(BigInteger.ONE);
        while(true)
        {
            if (isPrimeBi(iNum))
            {
                return iNum;
            }
            iNum = iNum.add(BigInteger.ONE);
        }
    }

    /**
     *
     * @param number Input integer.
     * @param count Number of output primes.
     * @return A list of prime numbers.
     */
    private static List<BigInteger> nextPrimes(BigInteger number, int count)
    {
        BigInteger iNum = number.add(BigInteger.ONE);
        List<BigInteger> result = new ArrayList<>();

        int i = 0;
        while(i < count)
        {
            if (isPrimeBi(iNum))
            {
                result.add(iNum);
                i++;
            }
            iNum = iNum.add(BigInteger.ONE);
        }

        return result;
    }

    /**
     * Application main.
     *
     * @param args Application arguments.
     */
    public static void main(String... args)
    {
        Scanner in = new Scanner(System.in);
        BigInteger number;

        while(true)
        {
            System.out.print("Enter an integer: ");
            try
            {
                number = in.nextBigInteger();
                System.out.format("First prime number greater than %d is %d. \n", number, nextPrime(number));
            }
            catch (Exception e)
            {
                System.out.println("Exit");
                break;
            }
        }
    }
}
